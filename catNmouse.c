///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Christian Li <lichrist@hawaii.edu>
/// @date    22_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>

#define DEFAULT_MAX_NUMBER (2048)

int main( int argc, char* argv[] ) {

   int theMaxValue;
   int theNumberImThinkingOf;
   int lower;
   int n = atoi( argv[1] );

   //if n>= 0 then the max value = n or else the max will equal to the default max
   if(n>=1 && n<= DEFAULT_MAX_NUMBER)
   {
      theMaxValue = n;
   }
   else if(argc < 2)
   {
      printf("Missing argument!");
      return 1; //Exits the program
   }
   else
   {
      theMaxValue = DEFAULT_MAX_NUMBER;
   }
   
   


   //Random number generator
   srand(time(NULL));
   theNumberImThinkingOf = rand()%(theMaxValue-lower+1);


   int aGuess;
   do
   {
      printf( "Ok cat, I'm thinking of a number from 1 to %d. Make a guess: \n",theMaxValue );
      scanf("%d", &aGuess);
      if(aGuess < 1)
      {
         printf("You must enter a number that's >= 1\n");
      }
      if(aGuess > theMaxValue)
      {
         printf("You must enter a number that's <= %d\n", theMaxValue);
      }
      if(aGuess > theNumberImThinkingOf)
      {
         printf("No cat... the number I’m thinking of is smaller than  %d\n", aGuess);
         //break;
      }
      if(aGuess < theNumberImThinkingOf)
      {
         printf("No cat... the number I’m thinking of is larger than %d\n", aGuess); 
         //break;
      }
      if(aGuess == theNumberImThinkingOf)
      {
         printf("You Got me.\n");
         printf("|\\---/|\n");
         printf("| o_o |\n");
         printf(" \\_^_/\n");
         break;
      }
   }
      while(1);




}

